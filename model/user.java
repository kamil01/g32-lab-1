package model;

public class user 
{
	//initialising variables for User
	private String username; //will be stored as an email
	private String password;
	private String first_name;
	private String last_name;
	private String mobile_number;
	
	//constructor for user
	public user(String username, String password, String first_name, String last_name, String mobile_number) {
		super();
		this.username = username;
		this.password = password;
		this.first_name = first_name;
		this.last_name = last_name;
		this.mobile_number = mobile_number;
	}
	
	//usernanme get/set
	public String getUsername() {
		return username; }
	public void setUsername(String username) {
		this.username = username; }
	
	//passsord get/set
	public String getPassword() {
		return password; }
	public void setPassword(String password) {
		this.password = password; }
	
	//firstname get/set
	public String getFirst_name() {
		return first_name; }
	public void setFirst_name(String first_name) {
		this.first_name = first_name; }
	
	//surname get/set
	public String getLast_name() {
		return last_name; }
	public void setLast_name(String last_name) {
		this.last_name = last_name; }
	
	//mobile number get/set
	public String getMobile_number() {
		return mobile_number; }
	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number; }
	
	public String toString()
	{
		return username + ", " + password + ", " + first_name + ", " + last_name + ", " + mobile_number;
	}
	
	
}
